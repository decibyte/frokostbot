# Frokostbot Roadmap

## Release an MVP that post today's menu to Slack.
    Requirements:
     [x] Extract menu from SharePoint.
     [x] Process menu to improve presentation with emojis.
     [x] Post menu to Slack.

## Improve the post by including a photo of the menu.
    Requirements:
     [x] Ask Google Translate to translate menu from Danish to English.
     [x] Ask Crayion to generate images of the menu in English.
     [x] Include a random image from the Crayion results as an attachment in
         the Slack post.

## Improve the post with an extended description.
    Requirements:
     [ ] Ask ChatGPT (or similar service) to provide a ~50 word, mouthwatering
         presentation of today's menu.
     [ ] Include the presentation in the Slack post.
